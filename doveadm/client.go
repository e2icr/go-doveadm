package doveadm

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
)

// A Client manages communication with the Doveadm API.
type Client struct {
	client *http.Client // HTTP client used to communicate with the API.

	baseURL   *url.URL
	authToken string

	Mailbox *MailboxCmd
	Flag    *FlagCmd
}

func NewClient(httpClient *http.Client) *Client {
	if httpClient == nil {
		httpClient = http.DefaultClient
	}

	c := &Client{client: httpClient}
	c.Mailbox = NewMailboxCmd(c)
	c.Flag = NewFlagCmd(c)
	return c
}

func (c *Client) SetBase(u string) *Client {
	b, err := url.Parse(u)
	if err != nil {
		panic(err)
	}
	c.baseURL = b
	return c
}

func (c *Client) SetAuth(token string) *Client {
	c.authToken = token
	return c
}

func (c *Client) Cmd(cmd string, params interface{}, val interface{}) (*http.Response, error) {

	// marshal params to json
	var buf io.ReadWriter
	if params != nil {
		buf = new(bytes.Buffer)
		err := json.NewEncoder(buf).Encode([]interface{}{
			[]interface{}{
				cmd,
				params,
				"",
			},
		})
		if err != nil {
			return nil, err
		}
	}

	// prepare request object
	req, err := http.NewRequest("POST", c.baseURL.String(), buf)
	if err != nil {
		return nil, err
	}
	req.Header.Add("Authorization", c.authToken)
	req.Header.Add("Content-Type", "application/json")

	// make request
	resp, err := c.client.Do(req)
	if err != nil {
		return resp, err
	}

	// status code check
	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("Unknown status [%d]", resp.StatusCode)
	}

	defer resp.Body.Close()

	// unmarshal json response
	var v [][]json.RawMessage
	err = json.NewDecoder(resp.Body).Decode(&v)
	if err == io.EOF {
		err = nil // ignore EOF errors caused by empty response body
	}
	if err != nil {
		return nil, fmt.Errorf("decode error: %q", err)
	}

	// check for succesful response code from doveadm
	status := string(v[0][0])
	if status != "\"doveadmResponse\"" {
		return resp, fmt.Errorf("invalid response: %+v", string(v[0][1]))
	}
	err = json.Unmarshal(v[0][1], &val)
	if err != nil {
		return nil, fmt.Errorf("unmarshal error %q", err)
	}

	return resp, err
}
