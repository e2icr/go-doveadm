package doveadm

import (
	"fmt"
	"testing"
)

var c *Client

func init() {
	c = NewClient(nil)
	c.SetBase("http://likebangla.com:3000/doveadm/v1")
	c.SetAuth("X-Doveadm-API N0Q5cG92VWk5czY1S0tia0NVN2NGckhscHdzSEZMZUI=")
}
func TestMailboxStatus(t *testing.T) {
	p := MailboxStatusParams{
		User:  "postmaster@likebangla.com",
		Field: []string{"messages"},
		Mask:  []string{"INBOX"},
	}
	status, err := c.Mailbox.Status(p)
	if err != nil {
		t.Error(err)
	}
	fmt.Println(status.Messages)
}

func TestAddFlag(t *testing.T) {
	p := FlagParams{
		User:  "postmaster@likebangla.com",
		Query: []string{"HEADER", "X-UID", "b77ldfcqcotg00c9j4h0"},
		Flag:  []string{"$query"},
	}
	err := c.Flag.Add(p)
	if err != nil {
		t.Error(err)
	}
}
func TestRemoveFlag(t *testing.T) {
	p := FlagParams{
		User:  "postmaster@likebangla.com",
		Query: []string{"HEADER", "X-UID", "b77ldfcqcotg00c9j4h0"},
		Flag:  []string{"$query"},
	}
	err := c.Flag.Remove(p)
	if err != nil {
		t.Error(err)
	}
}
func TestReplaceFlag(t *testing.T) {
	p := FlagParams{
		User:  "postmaster@likebangla.com",
		Query: []string{"HEADER", "X-UID", "b77ldfcqcotg00c9j4h0"},
		Flag:  []string{"\\Seen"},
	}
	err := c.Flag.Replace(p)
	if err != nil {
		t.Error(err)
	}
}
func TestSearch(t *testing.T) {
	p := SearchParams{
		User:  "postmaster@likebangla.com",
		Query: []string{"HEADER", "X-UID", "b77ldfcqcotg00c9j4h0"},
	}
	resp, err := c.Search(p)
	if err != nil {
		t.Error(err)
	}
	fmt.Printf("%+v", resp)
}
func TestFetch(t *testing.T) {
	p := FetchParams{
		Field: []string{"uid", "flags"},
		User:  "postmaster@likebangla.com",
		Query: []string{"UID", "40"},
	}
	resp, err := c.Fetch(p)
	if err != nil {
		t.Error(err)
	}
	fmt.Printf("%+v", resp)
}
