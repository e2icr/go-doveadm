package doveadm

type FetchParams struct {
	User  User  `json:"user,omitempty"`
	Query Query `json:"query,omitempty"`
	Field Field `json:"field,omitempty"`
}

type FetchResponse struct {
	Body              string `json:"body,omitempty"`
	DateReceived      string `json:"date.received,omitempty"`
	DateSaved         string `json:"date.saved,omitempty"`
	DateSent          string `json:"date.sent,omitempty"`
	Flags             string `json:"flags,omitempty"`
	GUID              string `json:"guid,omitempty"`
	Hdr               string `json:"hdr,omitempty"`
	ImapBody          string `json:"imap.body,omitempty"`
	ImapBodystructure string `json:"imap.bodystructure,omitempty"`
	ImapEnvelope      string `json:"imap.envelope,omitempty"`
	Mailbox           string `json:"mailbox,omitempty"`
	MailboxGuid       string `json:"mailbox-guid,omitempty"`
	Pop3UIDL          string `json:"pop3.uidl,omitempty"`
	Seq               string `json:"seq,omitempty"`
	SizePhysical      string `json:"size.physical,omitempty"`
	SizeVirtual       string `json:"size.virtual,omitempty"`
	Text              string `json:"text,omitempty"`
	TextUtf8          string `json:"text.utf8,omitempty"`
	UID               string `json:"uid,omitempty"`
	User              string `json:"user,omitempty"`
}

func (c *Client) Fetch(q FetchParams) ([]FetchResponse, error) {
	var resp []FetchResponse
	_, err := c.Cmd(
		"fetch",
		q,
		&resp,
	)
	if err != nil {
		return nil, err
	}
	return resp, nil
}
