package doveadm

type FlagCmd struct {
	cmd string
	c   *Client
}

func NewFlagCmd(c *Client) *FlagCmd {
	return &FlagCmd{
		c: c,
	}
}

type FlagParams struct {
	User  User  `json:"user,omitempty"`
	Query Query `json:"query,omitempty"`
	Flag  Flag  `json:"flag,omitempty"`
}
type FlagResponse struct {
}

func (cmd *FlagCmd) Add(q FlagParams) error {
	var resp []FlagResponse
	_, err := cmd.c.Cmd(
		"flagsAdd",
		q,
		&resp,
	)
	return err
}
func (cmd *FlagCmd) Remove(q FlagParams) error {
	var resp []FlagResponse
	_, err := cmd.c.Cmd(
		"flagsRemove",
		q,
		&resp,
	)
	return err
}
func (cmd *FlagCmd) Replace(q FlagParams) error {
	var resp []FlagResponse
	_, err := cmd.c.Cmd(
		"flagsReplace",
		q,
		&resp,
	)
	return err
}
