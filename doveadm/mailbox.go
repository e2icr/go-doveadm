package doveadm

type MailboxCmd struct {
	cmd string
	c   *Client
}

func NewMailboxCmd(c *Client) *MailboxCmd {
	return &MailboxCmd{
		c: c,
	}
}

type MailboxStatusParams struct {
	User  User        `json:"user,omitempty"`
	Field Field       `json:"field,omitempty"`
	Mask  MailboxMask `json:"mailboxMask,omitempty"`
}
type MailboxStatusResponse struct {
	Mailbox       string `json:"mailbox,omitempty"`
	Messages      string `json:"messages,omitempty"`
	Recent        string `json:"recent,omitempty"`
	UidNext       string `json:"uidnext,omitempty"`
	Unseen        string `json:"unseen,omitempty"`
	UidValidity   string `json:"uidvalidity,omitempty"`
	HighestModSeq string `json:"highestmodseq,omitempty"`
	VSize         string `json:"vsize,omitempty"`
	Guid          string `json:"guid,omitempty"`
}

func (cmd *MailboxCmd) Status(q MailboxStatusParams) (*MailboxStatusResponse, error) {
	var resp []MailboxStatusResponse
	_, err := cmd.c.Cmd(
		"mailboxStatus",
		q,
		&resp,
	)
	if err != nil {
		return nil, err
	}
	return &resp[0], nil
}
