package doveadm

type SearchParams struct {
	User  User  `json:"user,omitempty"`
	Query Query `json:"query,omitempty"`
}

type SearchResponse struct {
	MailboxGuid string `json:"mailbox-guid,omitempty"`
	Uid         string `json:"uid,omitempty"`
}

func (c *Client) Search(q SearchParams) ([]SearchResponse, error) {
	var resp []SearchResponse
	_, err := c.Cmd(
		"search",
		q,
		&resp,
	)
	if err != nil {
		return nil, err
	}
	return resp, nil
}
