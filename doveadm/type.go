package doveadm

type User string

type MailboxMask []string

type Field []string

type Flag []string

type Query []string
